package de.Bauernhofsimulator.Models;

import de.Bauernhofsimulator.Exceptions.BirthException;
import de.Bauernhofsimulator.Exceptions.DeathException;
import de.Bauernhofsimulator.Exceptions.PregnancyException;

import femaleNameGenerator.FemaleName;

public abstract class Animal {
	
	protected double personalRandom;
	
	protected String name;
	
	protected int age;


	protected double weight;
	
	protected boolean isPregnant;
	
	protected int pregnantMonth;
	
	
	
	
	/**
	 * Constructor
	 * 
	 * @param age
	 */
	public Animal(int age) {
		// set personalRandom
		// TODO: generiert eine Nummer zwischen 0.0 <= x < 1.0 (also niemals 1) ... sollte eventuell beachtet werden auch wenns kleinlich ist ;)
		this.personalRandom = Math.random(); 
		
		this.name = FemaleName.getRandomName();
		this.age = age;
		
		this.weight = this.calculateWeight();
	}
	
	
	/**
	 * Calculates and returns the weight of the animal.
	 * 
	 * @return double
	 */
	protected abstract double calculateWeight();
	
	/**
	 * Returns if the animal has died last month (used after aging).
	 * Returns true if animal is dead.
	 * Returns false if animal is still alive. Happy little animal!
	 * 
	 * @return boolean
	 */
	protected abstract boolean hasDied();
	
	
	/**
	 * Makes the animal one month older.
	 * @throws DeathException 
	 * @throws BirthException 
	 * @throws PregnancyException 
	 */
	public void ageOneMonth() throws DeathException, BirthException, PregnancyException {
		// make the animal older
		this.age++;
		
		// check if the animal has died, if so, throw DeathException
		if (this.hasDied()) {
			throw new DeathException(this);
		}
		
		// in case the animal is alive, calculate the new weight
		this.weight = this.calculateWeight();
		
		// handle the pregnancy, this might throw a BirthException
		this.handlePregnancy();
	}
	
	public String getName() {
		return name;
	}


	/**
	 * Handles the pregnancy. If birth was given, a BirthException is thrown.
	 * 
	 * @throws BirthException
	 * @throws PregnancyException 
	 */
	protected abstract void handlePregnancy() throws BirthException, PregnancyException;


	/**
	 * Returns age in years.
	 * 
	 * @return integer
	 */
	public int getAgeInYears() {
		return (int) Math.floor(this.age/12);
	}
	
	public String toString(){
		return name;	
	}
	
	/**
	 * Returns age in month.
	 * 
	 * @return integer
	 */
	public int getAge() {
		return age;
	}


	public double getWeight() {
		return this.weight;
	}
	
	public abstract String getType();
	

}
