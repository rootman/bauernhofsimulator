package de.Bauernhofsimulator.Models;

import de.Bauernhofsimulator.Exceptions.BirthException;
import de.Bauernhofsimulator.Exceptions.DeathException;
import de.Bauernhofsimulator.Exceptions.PregnancyException;

public class Cow extends Animal {
	
	private boolean givesMilk;
	private double totalMilk;
	private double monthMilk;

	public Cow(int age) {
		super(age);
		
		// newborns have a fixed weight, not a calculated one (this could also be handled in calculateWeight())
		if (age == 0) {
			this.weight = 20;
		}

		this.givesMilk = false;
		this.totalMilk = 0;
		this.monthMilk = 0;
	}
	
	@Override
	protected double calculateWeight() {
		int intAge = this.getAgeInYears();
		return (-0.01 * Math.pow(intAge, 4)
				+ 0.2 * Math.pow(intAge, 3)
				- 2.8 * Math.pow(intAge, 2)
				+ 77.5 * intAge
				+ 200 * this.personalRandom);
	}

	@Override
	protected boolean hasDied() {
		int intAge = this.getAgeInYears();
		double p = 1.0 / 264000 * Math.pow(Math.E, intAge / 2);
		double r = Math.random();
		return (r < p);
	}

	@Override
	protected void handlePregnancy() throws BirthException, PregnancyException {
		
		// the pig has to be older than 12 month
		if (this.age >= 12) {
			
			if (!this.isPregnant) {
				
				double r = Math.random();
				
				if (r < 0.02) {
					// become pregnant
					this.isPregnant = true;
					this.pregnantMonth = 0; // should be 0 anyway, but for now just make sure
					
					throw new PregnancyException();
				}
				
			} else {
				
				this.pregnantMonth++;
				
				if (this.pregnantMonth == 9) {
					
					// stop pregnancy
					this.isPregnant = false;
					
					// reset the pregnancy month
					this.pregnantMonth = 0;
					
					// this cow now produces milk
					this.givesMilk = true;
					
					// birth is a nice exception
					BirthException e = new BirthException();
					
					// add the little cow
					e.add(new Cow(0));

					// we need to actually throw the exception
					throw e;		
				}
			}
			
		}
		
	}
	
	@Override
	public void ageOneMonth() throws DeathException, BirthException, PregnancyException {
		// make the animal older
		this.age++;
		
		// check if the animal has died, if so, throw DeathException
		if (this.hasDied()) {
			throw new DeathException(this);
		}
		
		// in case the animal is alive, calculate the new weight
		this.weight = this.calculateWeight();
		
		// as this is a cow, we need to handle the milk
		this.handleMilk();

		// handle the pregnancy, this might throw a BirthException and a PregnancyException
		this.handlePregnancy();		
	}

	
	/**
	 * Handles the production of milk.
	 */
	private void handleMilk() {
		if (this.givesMilk) {
			this.monthMilk = 1000.0 * (3 + 2 * this.personalRandom) / 12;
			this.totalMilk = this.totalMilk + this.monthMilk;
		}
	}
	
	
	/**
	 * Returns the amount of Milk the cow produced last month.
	 * 
	 * @return double
	 */
	public double getMonthMilk() {
		return this.monthMilk;
	}
	
	public String toString(){
		if(this.age < 12) {
			return "Kalb " + this.name + " (" + this.getAgeInYears() + " Jahre)";
		}
		return "Kuh " + this.name + " (" + this.getAgeInYears() + " Jahre)";	
	}

	@Override
	public String getType() {
		if (this.age < 12) {
			return "Kalb";
		}
		return "Kuh";
	}

}
