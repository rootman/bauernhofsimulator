package de.Bauernhofsimulator.Models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import de.Bauernhofsimulator.Exceptions.BirthException;
import de.Bauernhofsimulator.Exceptions.DeathException;
import de.Bauernhofsimulator.Exceptions.PregnancyException;

public class Bauernhof {
	
	/**
	 * Holds the total amount of milk produced.
	 */
	private double milk;
	
	/**
	 * Animals living on the farm.
	 */
	private ArrayList<Animal> animals = new ArrayList<Animal>();
	
	/**
	 * Returns all (living) animals.
	 * 
	 * @return ArrayList<Animal>
	 */
	public ArrayList<Animal> getAnimals() {
		return animals;
	}

	/**
	 * Adds a cow to the farm.
	 * 
	 * @param age
	 */
	public void addCow(int age) {
		this.animals.add(new Cow(age));
	}
	
	/**
	 * Adds a pig to the farm.
	 * 
	 * @param age
	 */
	public void addPig(int age) {
			this.animals.add(new Pig(age));
	}
	
	/**
	 * Simulates the farm over a given period of time.
	 * 
	 * @param month
	 */
	public void simulate(int month) {
		
		// this will hold newborns, because they can't be added to this.animals directly in the animals loop
		ArrayList<Animal> children = new ArrayList<Animal>();
		
		for (int m = 1; m <= month; m++) {
			
			// reporting stuff
			System.out.println("=====================================================");
			System.out.println("Monat: " + m);
			//System.out.println("Tiere: " + this.animals.size() + " (vor Geburt)");
			
			// loop through all animals on the farm
			for (Iterator<Animal> i = this.animals.iterator(); i.hasNext(); ) {
				
				Animal animal = i.next();
				
				try {
					
					// let the animal age
					animal.ageOneMonth();
					
				} catch (DeathException e) {
					
					// the animal died, remove from list
					i.remove();
					
					// report it
					System.out.println(e.getMessage());
					
					// and move on to the next animal
					continue;
					
				} catch (BirthException e) {
					
					// get all the new born animals from the BirthException
					for (Iterator<Animal> j = e.getAnimals().iterator(); j.hasNext(); ) {
						
						// get the actual animal
						Animal child = j.next();
						
						// print the good news
						System.out.println(child + " wurde geboren.");
						
						// and add them to the list of newborns
						children.add(child);
						
					}
					
				} catch (PregnancyException e) {
					
					// report it
					System.out.println(animal + " ist tr�chtig geworden.");
					
				}
				
				// handle the milk
				if (animal instanceof Cow) {
					
					// cast animal to cow
					Cow cow = (Cow) animal;
					
					// add the milk to our tanks
					this.milk = this.milk + cow.getMonthMilk();
					
				}
				
				
			}
			
			// now add all the children to animals
			for (Iterator<Animal> i = children.iterator(); i.hasNext(); ) {
				
				//add to list
				this.animals.add(i.next());
				
				// remove from children
				i.remove();
				
			}
			
			// report
			System.out.println("Tiere: " + this.animals.size());
			System.out.println("Milch: " + this.milk);
			
		}
		
	}
	
	public void listAnimals(Comparator<Animal> comp) {
		Collections.sort(this.animals, comp);
		
		for (Iterator<Animal> i = this.animals.iterator(); i.hasNext(); ) {
			System.out.println(i.next());
		}
	}

}
