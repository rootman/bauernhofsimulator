package de.Bauernhofsimulator.Models;

import de.Bauernhofsimulator.Exceptions.BirthException;
import de.Bauernhofsimulator.Exceptions.PregnancyException;

public class Pig extends Animal {

	public Pig(int age) {
		super(age);
		
		// newborns have a fixed weight, not a calculated one (this could also be handled in calculateWeight())
		if (age == 0) {
			this.weight = 1.4;
		}
	}

	@Override
	protected double calculateWeight() {
		int intAge = this.getAgeInYears();
		return ((1.0 + 0.25 * this.personalRandom) * (0.0078 * Math.pow(intAge, 5) 
				- 0.33 * Math.pow(intAge, 4) 
				+ 5.3 * Math.pow(intAge, 3) 
				- 41 * Math.pow(intAge, 2) 
				+ 150 * intAge + 2.4));
	}

	@Override
	protected boolean hasDied() {
		int intAge = this.getAgeInYears();
		double p = Math.pow(intAge, 2) / 100000 * Math.sqrt(Math.pow(intAge, 3));
		double r = Math.random();
		return (r < p);
	}

	
	@Override
	protected void handlePregnancy() throws BirthException, PregnancyException {
		
		// the pig has to be older than 12 month
		if (this.age >= 12) {
			
			if (!this.isPregnant) {
				
				double r = Math.random();
				
				if (r < 0.01) {
					// become pregnant
					this.isPregnant = true;
					this.pregnantMonth = 0; // should be 0 anyway, but for now just make sure
					
					throw new PregnancyException();
				}
				
			} else {
				
				this.pregnantMonth++;
				
				if (this.pregnantMonth == 4) {
					
					//System.out.println("pig born"); // debug
					
					// stop pregnancy
					this.isPregnant = false;
					
					// reset the pregnancy month
					this.pregnantMonth = 0;
					
					// how many children?!
					// TODO check if we should use floor or round, check this again anyways as there are many many pigs
					int k = (int) Math.floor(36.0 / Math.PI * Math.pow(Math.E, (-1/2) * Math.pow(4 * Math.random() - 2, 2)));
					
					// birth is a nice exception
					BirthException e = new BirthException();
					
					// add all little pigs to the exception
					for (int i = 0; i < k; i++) {
						e.add(new Pig(0));
					}
					
					// we need to actually throw the exception
					throw e;		
				}
			}
			
		}
		
	}
	
	public String toString() {
		if(this.age < 12) {
			return "Ferkel " + this.name + " (" + this.getAgeInYears() + " Jahre)";
		}
		return "Sau " + this.name + " (" + this.getAgeInYears() + " Jahre)";	
	}
	
	@Override
	public String getType() {
		if (this.age < 12) {
			return "Ferkel";
		}
		return "Sau";
	}

}
