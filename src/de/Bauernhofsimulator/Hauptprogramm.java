package de.Bauernhofsimulator;


import de.Bauernhofsimulator.Comparators.AgeComparator;
import de.Bauernhofsimulator.Comparators.NameComparator;
import de.Bauernhofsimulator.Comparators.TypeComparator;
import de.Bauernhofsimulator.Comparators.WeightComparator;
import de.Bauernhofsimulator.Models.Animal;
import de.Bauernhofsimulator.Models.Bauernhof;

public class Hauptprogramm {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Bauernhof hof = new Bauernhof();
		
		hof.addCow(23);
		hof.addCow(13);
		hof.addPig(22);
		hof.addPig(21);
		
		hof.simulate(120);
		
		//hof.listAnimals(new AgeComparator());
		//hof.listAnimals(new NameComparator());
		hof.listAnimals(new TypeComparator());
		//hof.listAnimals(new WeightComparator());
		

	}

}
