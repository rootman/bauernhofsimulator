package de.Bauernhofsimulator.Tests;

import static org.junit.Assert.*;
import de.Bauernhofsimulator.Models.Cow;
import de.Bauernhofsimulator.Models.Pig;

public class Test {

	@org.junit.Test
	public void testGetTypePig1() {
		Pig pig = new Pig(11);
		assertEquals("Young pigs are called Ferkel", "Ferkel", pig.getType());
	}
	
	@org.junit.Test
	public void testGetTypePig2() {
		Pig pig = new Pig(12);
		assertEquals("Pigs older than one year are called Sau", "Sau", pig.getType());
	}
	
	@org.junit.Test
	public void testGetTypeCow1() {
		Cow cow = new Cow(11);
		assertEquals("Young cows are called Kalb", "Kalb", cow.getType());
	}
	
	@org.junit.Test
	public void testGetTypeCow2() {
		Cow cow = new Cow(12);
		assertEquals("Cows older than one year are called Kuh", "Kuh", cow.getType());
	}
	
	@org.junit.Test
	public void testPigWeight() {
		Pig pig = new Pig(0);
		assertEquals("Newly born pigs have a weight of 1.4kg", 1.4, pig.getWeight(), 0);
	}
	
	@org.junit.Test
	public void testCowWeight() {
		Cow cow = new Cow(0);
		assertEquals("Newly born cows have a weight of 20kg", 20, cow.getWeight(), 0);
	}

}
