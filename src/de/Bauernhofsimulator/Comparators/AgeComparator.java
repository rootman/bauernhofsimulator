package de.Bauernhofsimulator.Comparators;

import java.util.Comparator;

import de.Bauernhofsimulator.Models.Animal;

public class AgeComparator implements Comparator<Animal> {

	@Override
	public int compare(Animal arg0, Animal arg1) {
		return arg0.getAge() - arg1.getAge();
	}

}
