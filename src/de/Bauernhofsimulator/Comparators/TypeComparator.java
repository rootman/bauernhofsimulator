package de.Bauernhofsimulator.Comparators;

import java.util.Comparator;

import de.Bauernhofsimulator.Models.Animal;

public class TypeComparator implements Comparator<Animal> {

	@Override
	public int compare(Animal arg0, Animal arg1) {
		//return arg0.getClass().getSimpleName().compareTo(arg1.getClass().getSimpleName());
		return arg0.getType().compareTo(arg1.getType()); // works as long as the first thing in toString is the type. if we change this, we need to implement sth like getType()
	}

}
