package de.Bauernhofsimulator.Comparators;

import java.util.Comparator;

import de.Bauernhofsimulator.Models.Animal;

public class WeightComparator implements Comparator<Animal> {

	@Override
	public int compare(Animal arg0, Animal arg1) {
		if ((arg0.getWeight() - arg1.getWeight()) > 0) {
			return 1;
		} else if ((arg0.getWeight() - arg1.getWeight()) < 0) {
			return -1;
		}
		return 0;
	}

}
