package de.Bauernhofsimulator.Comparators;

import java.util.Comparator;

import de.Bauernhofsimulator.Models.Animal;

public class NameComparator implements Comparator<Animal> {

	@Override
	public int compare(Animal arg0, Animal arg1) {
		return arg0.getName().compareTo(arg1.getName());
	}

}
