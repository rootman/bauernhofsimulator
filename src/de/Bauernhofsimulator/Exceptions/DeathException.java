package de.Bauernhofsimulator.Exceptions;

import de.Bauernhofsimulator.Models.Animal;

public class DeathException extends Exception {

	private Animal animal;
	
	public DeathException(Animal animal) {
		this.animal = animal;
	}
	
	public String getMessage() {
		return this.animal + " ist verstorben";
	}
	
}
