package de.Bauernhofsimulator.Exceptions;

import java.util.ArrayList;

import de.Bauernhofsimulator.Models.Animal;

public class BirthException extends Exception {
	
	private ArrayList<Animal> animals = new ArrayList<Animal>();
	
	public void add(Animal animal) {
		this.getAnimals().add(animal);
	}

	public ArrayList<Animal> getAnimals() {
		return animals;
	}
	
}
